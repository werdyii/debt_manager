class DebtsController < ApplicationController
  def index
  	@debts = Debt.all
  end

  def new
  	@debt = Debt.new
  end

  def create
  	@debt = Debt.new params[:debt]

  	if @debt.save
  		redirect_to debts_path
  	else
  		render :new
  	end
  end

  def edit
  	@debt = Debt.find params[:id]
  end

  def update
  	@debt = Debt.find params[:id]
  	# @debt.name = params[:debt][:name]
  	# @debt.amount = params[:debt][:amount]
    params[:debt][:repaid] = params[:debt][:repaid].to_i + params[:splatka].to_i

  	if @debt.update_attributes params[:debt]
	  	# if @debt.save
	  	# @debt.repaid = @debt.repaid + params[:splatka].to_i
	  	# @debt.save
			# @debt.update_attribute :repaid, @debt.repaid + params[:splatka].to_i
  		redirect_to debts_path
  	else
  		render :edit
  	end
  end

  def destroy
    @album = Debt.find params[:id]
    @album.destroy
    redirect_to debts_path
  end
end
