class Debt < ActiveRecord::Base
  attr_accessible :amount, :name, :repaid

  validates :amount, :numericality => { :greater_than => 0 }
  validates :repaid, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => :amount }
  validates :name, :presence => true

  def percento
  	( self.repaid / self.amount ) * 100.0
  end

end
