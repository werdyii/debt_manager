DebetManager::Application.routes.draw do
  resources :debts
  root :to => 'debts#index'
end
