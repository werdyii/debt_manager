class CreateDebts < ActiveRecord::Migration
  def change
    create_table :debts do |t|
      t.string :name
      t.decimal :amount, :precision => 8, :scale => 2
      t.decimal :repaid, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
